#!/bin/bash

set -e

device="$1"

[ -n "$device" ] || {
    echo "Usage: ./shebang.sh DEVICE"
    echo "Available devices:"

    for e in setup/*.env; do
        echo "* "$(basename $e .env)
    done

    exit 1
}

device_env=setup/"${device}".env

# set up the .env file with a symlink to the device environment
if [ ! -e .env ] || [ -L .env ]; then
    [ -f "$device_env" ] || {
        echo "Device environment not found: $device_env"
        exit 1
    }
    echo "Using environment file: $device_env"
    ln -sf $device_env .env
elif [ -e .env ]; then
    echo "Reusing existing .env file"
fi

echo "------------------------"
cat .env
echo "------------------------"

# fuse is needed when setting up the chroot inside the Docker image
sudo modprobe fuse

# use local cache directory to persist when containers get removed
mkdir -p cache

# use local chroot directory as well
echo "Local chroot directory: chroot-$device"
mkdir -p chroot-"$device"

# build and start the Docker container
docker-compose up --build -d
docker-compose ps

# set up the Chrome OS SDK chroot in the container
docker-compose exec cros-sdk /bin/bash create-chroot.sh

exit 0
