Chromebook Firmware Utilities
-----------------------------

# Installing

This repository contains tools to build and flash Chromebook firmwares.  It
uses Git LFS in order to manage binary files, so please follow these steps to
be able to use it:

```
sudo apt install git-lfs  # on Debian...
git clone https://gitlab.collabora.com/chromium/flash-tools.git
cd flash-tools
git lfs fetch
git lfs checkout
```

# Flashing

The [`servoflash.py`](https://gitlab.collabora.com/chromium/flash-tools/-/blob/master/servoflash.py) tool can be used to flash firmware images onto Chromebook
devices using a Servo interface.  It uses a specific `flashrom` binary with its
library dependencies copied from the Chromium OS SDK.  They can be found in the
local `bin` and `lib` directories in this repository.

To flash a firmware image, the target device needs to be in the Google Servo
config file which is typically `/etc/google-servo.conf`.  This is where the
device serial numbers are associated with their names.  There also needs to be
a `servod` service running to be able to access the device.

Here's a sample command to flash a firmware, given all the preconditions
mentioned above are met:

```
./servoflash.py \
  --device=rk3399-gru-kevin-cbg-0 \
  --firmware=depthcharge-rk3399-gru-kevin-20180806.dev.bin
```

It can take a few minutes.  There should be these messages around the end,
which can vary depending on the type of Chromebook:

```
Erasing and writing flash chip... Verifying flash... VERIFIED.
SUCCESS
```

# Building

The Chromebook firmware needs to be rebuilt and flashed with extra patches and
configuration options turned on in order to enable the serial console in the
bootloader and boot with a kernel image and ramdisk supplied over TFTP
interactively.  The standard firmware shipped with the products is only
configured to boot the Chrome OS image present on the device, with no serial
console and no way to override it.  This is not suitable for automating the
Chromebook in a test lab such as LAVA, or for doing low-level kernel
development.

Building the firmware for a Chromebook can be non-trivial.  To make things
easier, this repository contains a Docker container and some helper scripts to
build firmware images for some known device types.

The
[`cros-build/bootstrap.sh`](https://gitlab.collabora.com/chromium/flash-tools/-/blob/master/cros-build/bootstrap.sh)
script can be used to create local directories, then build the Docker container
which will be using them and start it.  It takes only one argument which is the
device type.  For example:

```
./bootstrap.sh octopus
```
